import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import thunkMiddleware from 'redux-thunk';
import { makeRestState, makeRestReducer, makeRestActions} from './rest-helper.js';

const initialState = {
  'users':makeRestState(),
  'organizations':makeRestState(),
  'countries':makeRestState(),
};

const reducers = {
  'users':makeRestReducer('users', {'resetManyOnFetch':false}),
  'organizations':makeRestReducer('organizations', {'resetManyOnFetch':false}),
  'countries':makeRestReducer('countries', {'resetManyOnFetch':false}),
};

const actions = {
  'users':makeRestActions('users'),
  'organizations':makeRestActions('organizations'),
  'countries':makeRestActions('countries'),
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  combineReducers(reducers), 
  initialState, 
  composeEnhancers(
    applyMiddleware(
      thunkMiddleware,
      promiseMiddleware()
    )
  )
);
store.actions = actions;

window.store = store;

export default store;