import { connect as reduxConnect } from 'react-redux';

export function makeRestStateMap(tables) {
  return function(state) {
    var data = {};
    for (var i=0; i<tables.length; i++) {
      data[tables[i]] = state[tables[i]];
    }
    return data;
  }
}

export function makeRestActionsMap(tables) {
  return function(dispatch){
    var actions = {};
    for (var i=0; i<tables.length; i++) {
      let name = tables[i];
      actions[name] = {
        fetchOne: (id) => {
          return dispatch(store.actions[name].fetchOne(id))
        },
        fetchMany: (meta) => {
          return dispatch(store.actions[name].fetchMany(meta))
        },
        insert: (data) => {
          return dispatch(store.actions[name].insert(data))
        },
        update: (id, data) => {
          return dispatch(store.actions[name].update(id, data))
        },
        delete: (id) => {
          return dispatch(store.actions[name].delete(id))
        },
        toggle: (id, data) => {
          return dispatch(store.actions[name].toggle(id, data))
        }
      };
    }
    return {'actions':actions};
  }
}

export function makeRestState() {
  return {
    'one':{},
    'many':[],
    'manyMeta':{},
    'pending':{
      'fetchOne':false,
      'fetchMany':false,
      'insert':false,
      'update':false,
      'delete':false,
      'toggle':false
    },
    'error':{
      'fetchOne':false,
      'fetchMany':false,
      'insert':false,
      'update':false,
      'delete':false,
      'toggle':false
    }
  };
}

export function makeRestActions(name, options={}) {
  var actions = {
    'fetchOne':function(id) {
      return {
        type: name+'__fetchOne',
        payload: require('axios').get('/'+name+'/'+id)
      };
    },
    'fetchMany':function(meta) {
      return {
        type: name+'__fetchMany',
        payload: require('axios').get('/'+name, {'method':'GET', 'params': meta})
      }
    },
    'insert':function(data) {
      return {
        type: name+'__insert',
        payload: require('axios').post('/'+name, data)
      }
    },
    'update':function(id, data) {
      return {
        type: name+'__update',
        payload: require('axios').put('/'+name+'/'+id, null, {'method':'PUT', 'data': data})
      }
    },
    'delete':function(id) {
      return {
        type: name+'__delete',
        payload: require('axios').delete('/'+name+'/'+id)
      }
    },
    'toggle':function(id, data) {
      return {
        type: name+'__toggle',
        payload: require('axios').patch('/'+name+'/'+id, data)
      }
    }
  }
  return actions;
}

export function makeRestReducer(name, options={}){
  return function(state = {}, action){
    switch(action.type) {
      case name+'__fetchOne_PENDING':
        console.log('Reducer:'+name+'__fetchOne_PENDING', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.fetchOne = true;
        newState.error.fetchOne = false;
        newState.one = {};
        return Object.assign({}, state, newState);
      case name+'__fetchOne_FULFILLED':
        console.log('Reducer:'+name+'__fetchOne_FULFILLED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.fetchOne = false;
        newState.error.fetchOne = false;
        newState.one = action.payload.data.data;
        return Object.assign({}, state, newState);
      case name+'__fetchOne_REJECTED':
        console.log('Reducer:'+name+'__fetchOne_REJECTED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.fetchOne = false;
        newState.error.fetchOne = String(action.payload.response.status) +': '+action.payload.response.data.message;
        return Object.assign({}, state, newState);
      case name+'__fetchMany_PENDING':
        console.log('Reducer:'+name+'__fetchMany_PENDING', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.fetchMany = true;
        newState.error.fetchMany = false;
        if (options.resetManyOnFetch !== false) {
          newState.many = [];
        }
        return Object.assign({}, state, newState);
      case name+'__fetchMany_FULFILLED':
        console.log('Reducer:'+name+'__fetchMany_FULFILLED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.fetchMany = false;
        newState.error.fetchMany = false;
        newState.many = action.payload.data.data;
        newState.manyMeta = action.payload.data.meta;
        return Object.assign({}, state, newState);
      case name+'__fetchMany_REJECTED':
        console.log('Reducer:'+name+'__fetchMany_REJECTED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.fetchMany = false;
        newState.error.fetchMany = String(action.payload.response.status) +': '+action.payload.response.data.message;
        return Object.assign({}, state, newState);
      case name+'__insert_PENDING':
        console.log('Reducer:'+name+'__insert_PENDING', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.insert = true;
        newState.error.insert = false;
        return Object.assign({}, state, newState);
      case name+'__insert_FULFILLED':
        console.log('Reducer:'+name+'__insert_FULFILLED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.insert = false;
        newState.error.insert = false;
        return Object.assign({}, state, newState);
      case name+'__insert_REJECTED':
        console.log('Reducer:'+name+'__insert_REJECTED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.insert = false;
        newState.error.insert = String(action.payload.response.status) +': '+action.payload.response.data.message;
        return Object.assign({}, state, newState);
      case name+'__update_PENDING':
        console.log('Reducer:'+name+'__update_PENDING', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.update = true;
        newState.error.update = false;
        return Object.assign({}, state, newState);
      case name+'__update_FULFILLED':
        console.log('Reducer:'+name+'__update_FULFILLED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.update = false;
        newState.error.update = false;
        
        // update the .many data if necessary
        for (var i=0; i<newState.many.length; i++) {
          
          if (newState.many[i].id == action.payload.data.data.id) {
            for (var key in action.payload.data.data) {
              newState.many[i][key] = action.payload.data.data[key];
            }
          }
        }
        
        // update the .one data if necessary
        if (newState.one.id == action.payload.data.data.id) {
          for (var key in action.payload.data.data) {
            newState.one[key] = action.payload.data.data[key];
          }
        }
        
        return Object.assign({}, state, newState);
      case name+'__update_REJECTED':
        console.log('Reducer:'+name+'__update_REJECTED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.update = false;
        newState.error.update = String(action.payload.response.status) +': '+action.payload.response.data.message;
        return Object.assign({}, state, newState);
      case name+'__delete_PENDING':
        console.log('Reducer:'+name+'__delete_PENDING', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.delete = true;
        newState.error.delete = false;
        return Object.assign({}, state, newState);
      case name+'__delete_FULFILLED':
        console.log('Reducer:'+name+'__delete_FULFILLED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.delete = false;
        newState.error.delete = false;
        return Object.assign({}, state, newState);
      case name+'__delete_REJECTED':
        console.log('Reducer:'+name+'__delete_REJECTED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.delete = false;
        newState.error.delete = String(action.payload.response.status) +': '+action.payload.response.data.message;
        return Object.assign({}, state, newState);
      case name+'__toggle_PENDING':
        console.log('Reducer:'+name+'__toggle_PENDING', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.toggle = true;
        newState.error.toggle = false;
        return Object.assign({}, state, newState);
      case name+'__toggle_FULFILLED':
        console.log('Reducer:'+name+'__toggle_FULFILLED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.toggle = false;
        newState.error.toggle = false;
        newState.many = state.many.slice();
        // update the .many data if necessary
        for (var i=0; i<newState.many.length; i++) {
          if (newState.many[i].id == action.payload.data.id) {
            for (var key in action.payload.data.data) {
              newState.many[i][key] = action.payload.data.data[key];
            }
          }
        }
        
        // update the .one data if necessary
        if (newState.one.id == action.payload.data.id) {
          for (var key in action.payload.data.data) {
            newState.one[key] = action.payload.data.data[key];
          }
        }
        return Object.assign({}, state, newState);

      case name+'__toggle_REJECTED':
        console.log('Reducer:'+name+'__toggle_REJECTED', action);
        var newState = {};
        newState = Object.assign({}, state);
        newState.pending.toggle = false;
        newState.error.toggle = String(action.payload.response.status) +': '+action.payload.response.data.message;
        return Object.assign({}, state, newState);
      default:
        return state;
    }
  }
}

export function connect(tables, component) {
  if (typeof(tables) == 'string') {
    tables = [tables];
  }
  return reduxConnect(makeRestStateMap(tables), makeRestActionsMap(tables))(component);
}