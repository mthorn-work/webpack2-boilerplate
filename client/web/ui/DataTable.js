import React, {Component} from 'react';
import Table from 'react-bootstrap/lib/Table';
import Pagination from 'react-bootstrap/lib/Pagination';
import InputGroup from 'react-bootstrap/lib/InputGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import ProgressBar from 'react-bootstrap/lib/ProgressBar';
import { Link } from 'react-router';

export class DataTableColumn {
  constructor(label, name, width, sortable, renderFunction)
  {
    this.label = label;
    this.name = name;
    this.width = width;
    this.sortable = sortable;
    this.renderFunction = renderFunction;
  }
  
  static renderColumn(column, i) {
    return (<col key={i} width={column.width} />);
  }
  
  static renderHeader(column, i) {
    if (column.sortable === false) {
      return (<th key={i}>{column.label}</th>);
    } else {
      var sortIcon = 'chevron-right';
      if (this.state.sortColumn == column.name) {
        sortIcon = 'chevron-'+((this.state.sortDirection == 'asc')?'up':'down');;
      }
      return (<th key={i} style={{'cursor':'pointer'}} onClick={this.changeSort.bind(this, column.name)}><Glyphicon glyph={sortIcon} /> {column.label}</th>);
    }
  }
  
  static renderCell(column, j) {
    var data = this.data;
    if (typeof(column.renderFunction) == 'function') {
      var valueToShow = column.renderFunction(data, this, column, j);
    } else {
      var valueToShow = data[column.name];
    }
    return <td key={j}>{valueToShow}</td>
  }
}

export class DataTable extends Component {
  constructor(props) {
    super(props);
    this.title = false;
    this.addFormUrl = false;
    this.showSearch = false;
    this.searchTimeout = false;
    this.searchDelay = 500;
    this.columns = [];
    this.showProgressBarWhenPending = false;
    this.tableProps = {'striped':true, 'condensed':true, 'bordered':true, 'hover':true, 'responsive':true};
    this.paginationProps = {'first':true, 'last':true, 'maxButtons':5,};
  }
  
  componentWillReceiveProps(nextProps) {
    if (nextProps[this.table].manyMeta.lastRefresh > this.state.lastRefresh) {
      this.setState(nextProps[this.table].manyMeta);
    }
  }
  
  handleSearch(search) {
    if (typeof(search) == 'object') {
      search = search.target.value;
    } else {
      search = '';
    } 
    
    if (this.searchTimeout !== false) {
      window.clearTimeout(this.searchTimeout);
    }
    var newState = Object.assign({}, this.state);
    newState.search = search;
    this.setState(newState);
    
    var _this = this;
    this.searchTimeout = window.setTimeout(function(){
      _this.props.actions[_this.table].fetchMany(newState); 
    }, this.searchDelay);
  }
  
  changeSort(newColumn) {
    var newState = Object.assign({}, this.state);
    if (newState.sortColumn == newColumn) {
      newState.sortDirection = (newState.sortDirection == 'asc')?'desc':'asc'; 
    } else {
      newState.sortDirection = 'asc';
    }
    newState.sortColumn = newColumn;
    newState.currentPage = 0;
    this.setState(newState);
    this.props.actions[this.table].fetchMany(newState);  
  }
  
  changePage(newPage) {
    var newState = Object.assign({}, this.state);
    newState.currentPage = (newPage - 1);
    this.setState(newState);
    this.props.actions[this.table].fetchMany(newState);  
  }
  
  componentWillMount() {
    this.props.actions[this.table].fetchMany(this.state);
  }
  
  deleteItem(id) {
    var _this = this;
    this.props.actions[this.table].delete(id).then(function(){
      _this.props.actions[_this.table].fetchMany();
    });
  }
  
  toggle(id, field, newValue) {
    var data = {};
    data[field] = newValue;
    this.props.actions[this.table].toggle(id, data);
  }
  
  renderColgroup() {
    return (
      <colgroup>
        {this.columns.map(DataTableColumn.renderColumn, this)} 
      </colgroup>
    );
  }
  
  renderThead() {
    return (
      <thead>
        <tr>{this.columns.map(DataTableColumn.renderHeader, this)}</tr>
      </thead>
    );
  }
  
  renderTbody() {
    var _this = this;
    return (
      <tbody>
        {(this.props[this.table].pending.fetchMany === true && this.showProgressBarWhenPending === true) && <tr><td colSpan={this.columns.length}><ProgressBar bsStyle="info" now={100} active style={{'margin':'20px 5px'}} /></td></tr>}
        {this.props[this.table].many.map(data => { 
          _this.data = data;
          return (
            <tr key={data.id}>
              {this.columns.map(DataTableColumn.renderCell, _this)}                
            </tr>
          );
        })}
      </tbody>
    );
  }
  
  renderAddFormLink() {
    return (<Link to={this.addFormUrl}><button className="btn btn-primary"><Glyphicon glyph="plus" /></button></Link>);
  }
  
  renderPagination() {
    return (<Pagination className="pull-right" style={{'margin':'0px'}} {...this.paginationProps} items={this.state.maxPage} activePage={this.state.currentPage + 1} onSelect={this.changePage.bind(this)} />);
  }
  
  renderHeader() {
    return (
      <div className="panel-heading" style={{'padding':'5px 8px'}}>
        {this.title !== false && <strong style={{'lineHeight':'32px', 'verticalAlign':'middle','fontSize':'120%'}}>{this.title}</strong>}
        {this.showSearch !== false && 
        <InputGroup style={{'width':'210px'}} className="pull-right">
          <InputGroup.Addon>
            <Glyphicon glyph="search" />
          </InputGroup.Addon>
          <FormControl type="search" value={this.state.search} name="search" onChange={this.handleSearch.bind(this)} />
          <InputGroup.Addon onClick={this.handleSearch.bind(this, '')}>
            <Glyphicon glyph="remove" />
          </InputGroup.Addon>
        </InputGroup>}
        <div className="clearfix"></div>
      </div>
    );
  }
  
  renderFooter() {
    return (
      <div className="panel-footer" style={{'padding':'5px 8px'}}>
        {this.addFormUrl !== false && this.renderAddFormLink()}
        {this.renderPagination()}
        <div className="clearfix"></div>
      </div>
    );
  }
  
  render() {
    return (
      <div className="panel panel-primary">
        {this.renderHeader()}
        <Table {...this.tableProps} style={{'marginBottom':'0px'}}>
          {this.renderColgroup()}
          {this.renderThead()}
          {this.renderTbody()}
        </Table>
        {this.renderFooter()}
      </div>
    );
  }
}