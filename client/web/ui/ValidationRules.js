export function LengthRange(fieldName, fieldValue, rule) {
  
  var passes = true;
  if (typeof(rule.min) == 'number' && fieldValue.length < rule.min) {
    passes = false;
  }
  if (typeof(rule.max) == 'number' && fieldValue.length > rule.max) {
    passes = false;
  }

  if (passes === false) {
    if (typeof(rule.message) == 'string') {
      return rule.message;
    }
    var message = '';
    if (typeof(rule.min) == 'number'){ 
      message += ' at least '+String(rule.min);
    }
    if (typeof(rule.max) == 'number'){ 
      message += ((message == '')?'':' and ') + ' at most '+String(rule.max);
    }
    message += ' characters long';
    return 'Must be '+message;
  }
  return true;
}