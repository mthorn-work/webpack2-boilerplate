
import React, {Component} from 'react';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormControl from 'react-bootstrap/lib/FormControl';
import HelpBlock from 'react-bootstrap/lib/HelpBlock';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import Button from 'react-bootstrap/lib/Button';
import { hashHistory } from 'react-router'

export class EditForm extends Component {
  componentWillMount() {
    var _this = this;
    console.log('attempting to fetch one for ',this.table);
    if (this.props.params.id == 'new') {
      this.setState({'loading':false});
    } else {
      this.props.actions[this.table].fetchOne(this.props.params.id).then(function(){
        _this.setState({'data':_this.props[_this.table].one, 'loading':false});
      });
    }
  }
  
  updateValue(e) {
    var newState = Object.assign({}, this.state);
    newState.data[e.target.name] = e.target.value;
    this.setState(newState);
  }
  
  validateField(e, fieldName, fieldValue) {
    if (typeof(this.validationRules) != 'object') {
      return true;
    }
    if (e != null) {
      fieldName = e.target.name;
      fieldValue = e.target.value;
    }
    if (typeof(this.validationRules[fieldName]) != 'undefined') {
      var rules  = this.validationRules[fieldName];
      if ((rules  instanceof Array) === false) {
        rules = [rules];
      }
      for (var i=0;i<rules.length; i++) {
        var rule = rules[i];
        var messageOrPass = rule.function(fieldName, fieldValue, rule);
        if (messageOrPass !== true) {
          var newState = Object.assign({}, this.state);
          if (typeof(newState.validationMessages) != 'object') {
            newState.validationMessages = {};
          }
          newState.validationMessages[fieldName] = messageOrPass;
          this.setState(newState);
          return false;
        } else {
          var newState = Object.assign({}, this.state);
          if (typeof(newState.validationMessages) != 'object') {
            newState.validationMessages = {};
          }
          newState.validationMessages[fieldName] = messageOrPass;
          this.setState(newState);
          return true;
        }
      }
    }
  }
  
  formPassesValidation(formObj) {
    if (typeof(this.validationRules) != 'object') {
      return true;
    }
    
    function determineFieldValue(field) {
      //console.log('trying to determine value of field ', field, field.tagName, field.getAttribute('type'), field.getAttribute('value'), field.getAttribute('checked'));
      var tag = String(field.tagName).toLowerCase();
      var value = null;
      switch(tag) {
        case 'select':
          value = field.options[field.selectedIndex].value;
          break;
        case 'input':
          var type = String(field.getAttribute('type')).toLowerCase();
          switch (type) {
            case 'color':
            case 'text':
            case 'number':
            case 'password':
            case 'email':
            case 'hidden':
            case 'range':
            case 'search':
            case 'tel':
            case 'url':
            case 'date':
            case 'time':
            case 'week':
            case 'month':
            case 'datetime-local':
              value = field.getAttribute('value');
              break;
            case 'checkbox':
              value = (field.checked === true)
              break;
            case 'radio':
              console.log('on radio. not entirely sure what to do here. come back to it');
              break;
          }
          break;
        case 'textarea':
          value = field.getAttribute('value');
          break;
      }
      return value;
    }
    
    var formPasses = true;
    for(var fieldName in this.validationRules) {
      var fieldValue = determineFieldValue(formObj[fieldName]);
      var fieldResult = this.validateField(null, fieldName, fieldValue);
      if (fieldResult === false) {
        formPasses = false;
      }
    }
    return formPasses;
  }
  
  saveChanges(e) {
    e.preventDefault();
    if (this.formPassesValidation(e.target) === true) {
      var redirectUrl = this.redirectUrl;
      if (this.props.params.id == 'new') {
        this.props.actions[this.table].insert(this.state.data).then(function(){
          hashHistory.push(redirectUrl)
        });
      } else {
        this.props.actions[this.table].update(this.state.data.id, this.state.data).then(function(){
          hashHistory.push(redirectUrl)
        });
      }
      return false;
    } else {
      return false;
    }
  }
}

export class FieldGroup extends Component {
  constructor(props) {
    super(props);
    this.controlProps = {};
    this.layout = 'normal'; // alternatively, checkable.
    
    // build a list of properties that should be applied directly to the FormControl component
    for (var key in props) {
      if (key != 'label' && key != 'help' && key != 'value' && key != 'children' && key != 'validationMessage') {
        if (key == 'type' && props[key] == 'select') {
          this.controlProps['componentClass'] = 'select';
        } else {
          this.controlProps[key] = props[key];
        }
      }
    }
    
  }
  
  render() {
    // figure out if we should show an error message
    this.validationState={};
    this.validateMessage = false;
    if (typeof(this.props.validationMessage) == 'string' && String(this.props.validationMessage).length > 0) {
      this.validationState.validationState = 'error';
      this.validateMessage = this.props.validationMessage;
    }
    return (
      <FormGroup {...this.validationState}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl {...this.controlProps} value={this.props.value}>
          {this.props.children}
        </FormControl>
        {this.props.help && <HelpBlock>{this.props.help}</HelpBlock>}
        {this.validateMessage && <HelpBlock>{this.validateMessage}</HelpBlock>}
      </FormGroup>
    );
  }
}

export function SubmitCancelButtons({submitLabel, cancelLabel, ...props}) {
  return (
    <ButtonGroup className="pull-right">
      <Button bsStyle="warning" type="button" onClick={hashHistory.goBack}>Cancel</Button>
      <Button bsStyle="primary" type="submit">Submit</Button>
    </ButtonGroup>
  );
}