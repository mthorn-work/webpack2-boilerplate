import React, { Component } from 'react';
import Nav1 from './Nav1.js';

export default class App extends Component {
  render() {
    return (
      <div>
        <Nav1 />
        <div className="alert alert-success"><strong>React is up and running - with a rest backend!</strong></div>
        {this.props.children}
      </div>
    );
  }
}