import React, {Component} from 'react';
import Button from 'react-bootstrap/lib/Button';
import { connect } from '../../store/rest-helper.js';
import { FieldGroup, EditForm, SubmitCancelButtons } from '../ui/Forms.js';
import { LengthRange } from '../ui/ValidationRules.js';

class UsersView extends EditForm {
  constructor(props) {
    super(props);
    this.redirectUrl = '/users';
    this.table = 'users';
    
    this.validationRules = {
      'first_name':{'function':LengthRange, 'min':2, 'max':20},
      'last_name':{'function':LengthRange, 'min':2, 'max':20},
      'email':{'function':LengthRange, 'min':2, 'max':20},
    };
    
    this.state={
      'loading':true,
      'validationMessages':{},
      'data':{
        'first_name':'',
        'last_name':'',
        'email':'',
        'org_id':'',
      }
    };
  }
  
  componentWillMount() {
    super.componentWillMount();
    this.props.actions.organizations.fetchMany();
  }
  
  render() {
    var _this = this;
    return (
      <form onSubmit={this.saveChanges.bind(this)}>
        {this.state.loading && <h1>Loading...</h1>}
        {this.state.loading === false && 
          <div>
            <FieldGroup name="first_name" type="text" label="First Name" onKeyUp={this.validateField.bind(this)} onChange={this.updateValue.bind(this)} value={this.state.data.first_name} validationMessage={this.state.validationMessages.first_name} />
            <FieldGroup name="last_name" type="text" label="Last Name" onKeyUp={this.validateField.bind(this)} onChange={this.updateValue.bind(this)} value={this.state.data.last_name} validationMessage={this.state.validationMessages.last_name} />
            <FieldGroup name="email" type="email" label="E-mail" onKeyUp={this.validateField.bind(this)} onChange={this.updateValue.bind(this)} value={this.state.data.email} validationMessage={this.state.validationMessages.email} />
            <FieldGroup name="org_id" type="select" label="Organization" onKeyUp={this.validateField.bind(this)} onChange={this.updateValue.bind(this)} value={this.state.data.org_id} validationMessage={this.state.validationMessages.org_id}>
              <option value="">Select</option>
              {this.props.organizations.many.map(function(org, i){
                return (
                  <option key={i} value={org.id}>{org.name}</option>
                );
              })}
            </FieldGroup>
          </div>
        }
        <SubmitCancelButtons />
      </form>
    );
  }
}

export default connect(['users', 'organizations'], UsersView)
