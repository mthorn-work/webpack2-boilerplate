import React, { Component } from 'react';
import { Link } from 'react-router';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import { DataTable, DataTableColumn } from '../ui/DataTable.js';
import { connect } from '../../store/rest-helper.js';

class UsersList extends DataTable {
  constructor(props) {
    super(props);
    this.table = 'users';
    this.title = 'Users';
    this.addFormUrl = '/users/new';
    this.showSearch = true;
    this.state = {
      'sortColumn':'first_name', 
      'sortDirection':'asc',
      'currentPage':0,
      'pageSize':10,
      'lastRefresh':0,
      'search':'',
    }
    
    this.columns.push(new DataTableColumn('Name', 'first_name', '40%', true, function(data){
      return (<Link to={`/users/${data.id}`}>{data.first_name} {data.last_name}</Link>)
    }));
    this.columns.push(new DataTableColumn('E-mail', 'email', '40%', true, function(data){
      return (<Link to={`/users/${data.id}`}>{data.email}</Link>)
    }));
    this.columns.push(new DataTableColumn('Heart', 'is_hearted', '10%', true, function(data, table){
      let icon = (data.is_hearted == 1)?'heart':'heart-empty';
      return (<Glyphicon glyph={icon} onClick={table.toggle.bind(table, data.id, 'is_hearted', (data.is_hearted == 1)?0:1)} style={{'color':'red'}} />);
    }));
    this.columns.push(new DataTableColumn('', null, '10%', false, function(data, table){
      return (
        <Button bsStyle="warning" onClick={table.deleteItem.bind(table, data.id)}>
          <Glyphicon glyph="trash" />
        </Button>
      );
    }));
  }
}

export default connect('users', UsersList)
