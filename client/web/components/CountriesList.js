import React from 'react';
import { DataTable, DataTableColumn } from '../ui/DataTable.js';
import { connect } from '../../store/rest-helper.js';

class CountriesList extends DataTable {
  constructor(props) {
    super(props);
    this.table = 'countries';
    this.title = 'Countries';
    this.showSearch = true;
    this.state = {
      'sortColumn':'name', 
      'sortDirection':'desc',
      'currentPage':0,
      'pageSize':10,
      'lastRefresh':0,
      'search':'',
    }
    
    this.columns.push(new DataTableColumn('ID', 'id', '30%', true));
    this.columns.push(new DataTableColumn('Name', 'name', '70%', true, function(data, column, index){
      return (<b>{data.name}</b>);
    }));
  }
}

export default connect('countries', CountriesList)
