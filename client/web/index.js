import React, { Component} from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';
import App from './components/App.js';
import CountriesList from './components/CountriesList.js';
import UsersList from './components/UsersList.js';
import UsersView from './components/UsersView.js';
require('./styles/index.scss');
import store from '../store/store.js';
import { Provider } from 'react-redux';

var stage;
if (location.hostname == '127.0.0.1') {
  if (location.port == '3000') {
    stage = 'development';
  } else {
    stage = 'qa';
  }
} else {
  stage = 'production';
}

var axios = require('axios');
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.put['Content-Type'   ] = 'application/x-www-form-urlencoded';
axios.defaults.headers.post['Content-Type'  ] = 'application/x-www-form-urlencoded';

switch(stage) {
  case 'development':
    axios.defaults.baseURL = 'http://127.0.0.1:3001/api/v1';
    break;
  case 'qa':
    axios.defaults.baseURL = 'http://127.0.0.1:3005/api/v1';
    break;
  case 'production':
    //axios.defaults.baseURL = 'http://127.0.0.1:3001/api/v1';
    console.log('You must configure your axios baseURL for production in indexjs');
    break;
}

ReactDOM.render((
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={App}>
        <Route path="/countries" component={CountriesList} />
        <Route path="/users" component={UsersList} />
        <Route path="/users/:id" component={UsersView} />
      </Route>
    </Router>
  </Provider>
), document.getElementById('root'));
