<?php
namespace App;

class RestController 
{
    public function __construct() 
    {
        global $app;
        if (is_array($app) === true || is_object($app) === true) {
          $this->app = $app;
        } else {
          throw new \Exception('RestController requires that the silex app object be stored into a global variable named $app');
        }
    }
    
    public function getConfig($table)
    {
        $key = 'RestTable__' . $table;
        if (isset($this->app[$key]) === true) {
            return $this->app[$key];
        } else {
            http_response_code(404);
            exit(json_encode(['success'=>false, 'message'=>'table not configured']));
        }
    }
    
    public function addDebug($string)
    {
        if (isset($this->app['monolog']) === true) {
            $this->app['monolog']->debug($string);
        } else {
            error_log($string);
        }
    }
    
    protected function mergeParams()
    {
        $valuesInInput = json_decode(file_get_contents("php://input"), true);
        return array_merge(((is_array($valuesInInput) === true)?$valuesInInput:[]), $_REQUEST);
    }
    
    public function fetchMany($table)
    {
        $config = $this->getConfig($table);
        if ($config->doesAllowAction('fetchMany') === false) {
            return $this->app->json(['success'=>false, 'message'=>'fetchMany not allowed on this resource'], 403);
        }
        
        $orm = \ORM::for_table($table);
        
        $selectFields = $config->getSelectableFields();
        if (isset($selectFields[0]) === true && $selectFields[0] == '*') {
            # do nothing!
        } else {
            foreach($selectFields as $field) {
                $orm->select($field);
            }
        }
        
        $autoWheres = $config->getSelectAutoWhere();
        foreach($autoWheres as $field=>$value) {
            $orm->where($field, $value);
        }
        
        # apply searching
        if (isset($_REQUEST['search']) === true && $_REQUEST['search'] != '') {
            $searchableFields = $config->getSearchableFields();
            if (count($searchableFields) === 0) {
                return $this->app->json(['success'=>false, 'message'=>'fetchMany is not configured to allow search on this resource'], 403);
            }
            $terms = explode(' ', trim($_REQUEST['search']));
            foreach($terms as $term) {
                $term = trim($term);
                if ($term != '') {
                    $this->addDebug('searching for term: '.$term);
                    $rawWhere = '';
                    $rawTerms = [];
                    foreach($searchableFields as $field) {
                        $rawWhere .= ($rawWhere == '')?'':' OR ';
                        $rawWhere .= 'lower('.$field.') like ?';
                        $rawTerms[] = '%'.strtolower($term).'%';
                    }
                    
                    $orm->where_raw('('.$rawWhere.')', $rawTerms);
                }
            }
        }
        
        $meta = [
            'lastRefresh'=>time(),
        ];
        
        # apply sorting
        list($sortColumn, $sortDirection) = $config->getDefaultSorting();
        if (isset($_REQUEST['sortColumn']) === true) {
            $sortColumn = $_REQUEST['sortColumn'];
            $sortDirection = ($_REQUEST['sortDirection'] === 'desc')?'desc':'asc';
        }
        if ($sortColumn != '') {
            $meta['sortColumn'] = $sortColumn;
            $meta['sortDirection'] = $sortDirection;
            if($sortDirection == 'desc') {
                $orm->order_by_desc($sortColumn);
            } else {
                $orm->order_by_asc($sortColumn);
            }
        }
        
        # apply limit, page size, sorting
        $currentPage = (isset($_REQUEST['currentPage']) === true)?intval($_REQUEST['currentPage']):(-1);
        $pageSize    = (isset($_REQUEST['pageSize'])    === true)?intval($_REQUEST['pageSize']):10;
        
        if ($currentPage >= 0) {
          
            # we need to get the maxPage *before* we apply limit/offset
            $meta['maxPage'] = ceil($orm->count() / $pageSize);
            
            $orm->limit($pageSize);
            $orm->offset(($pageSize * $currentPage));
            $meta['currentPage'] = $currentPage;
            $meta['pageSize'] = $pageSize;
        }
        
        $data = $orm->find_array();
        
        $nonSelectFields = $config->getNonSelectableFields();
        for($i=0; $i<count($data); $i++) {
            foreach($nonSelectFields as $field) {
                unset($data[$i][$field]);
            }
        }
        
        $config->doSleep();
        
        return $this->app->json([
            'success'=>true, 
            'data'=>$data, 
            'meta'=>$meta
        ], 200);
    }
    
    public function fetchOne($table, $id)
    {
        $config = $this->getConfig($table);
        if ($config->doesAllowAction('fetchOne') === false) {
            return $this->app->json(['success'=>false, 'message'=>'fetchOne not allowed on this resource'], 403);
        }

        $orm = \ORM::for_table($table);
        
        $selectFields = $config->getSelectableFields();
        if (isset($selectFields[0]) === true && $selectFields[0] == '*') {
            # do nothing!
        } else {
            foreach($selectFields as $field) {
                $orm->select($field);
            }
        }
        
        $autoWheres = $config->getSelectAutoWhere();
        foreach($autoWheres as $field=>$value) {
            $orm->where($field, $value);
        }
        
        $data = $orm->find_one($id)->as_array();
        
        $nonSelectFields = $config->getNonSelectableFields();
        foreach($nonSelectFields as $field) {
            unset($data[$field]);
        }

        $config->doSleep();

        return $this->app->json(['success'=>true, 'data'=>$data], 200);
    }
    
    public function insertItem($table)
    {
        $config = $this->getConfig($table);
        if ($config->doesAllowAction('insert') === false) {
            return $this->app->json(['success'=>false, 'message'=>'insert not allowed on this resource'], 403);
        }

        $values = $config->getInsertableValues($this->mergeParams());
        $item = \ORM::for_table($table)->create();
        foreach($values as $key=>$value) {
            $item->$key = $value;
        }
        
        $autoWheres = $config->getInsertAutoWhere();
        foreach($autoWheres as $field=>$value) {
            $item->$field = $value;
        }
        
        $item->save();
        $item->id = $item->id();

        $config->doSleep();

        return $this->app->json(['success'=>true, 'data'=>$item->as_array()], 200);
    }
    
    public function updateItem($table, $id)
    {
        $config = $this->getConfig($table);
        if ($config->doesAllowAction('update') === false) {
            return $this->app->json(['success'=>false, 'message'=>'update not allowed on this resource'], 403);
        }

        $orm = \ORM::for_table($table);
        $autoWheres = $config->getSelectAutoWhere();
        foreach($autoWheres as $field=>$value) {
            $orm->where($field, $value);
        }
        $item = $orm->find_one($id);

        $values = $config->getUpdateableValues($this->mergeParams());
        foreach($values as $key=>$value) {
            if ($key != 'id') {
                $item->$key = $value;
            }
        }
        
        $autoWheres = $config->getUpdateAutoWhere();
        foreach($autoWheres as $field=>$value) {
            $item->$field = $value;
        }
        
        $item->save();

        $config->doSleep();

        return $this->app->json(['success'=>true, 'data'=>$item->as_array()], 200);
    }
    
    public function deleteItem($table, $id)
    {
        $config = $this->getConfig($table);
        if ($config->doesAllowAction('delete') === false) {
            return $this->app->json(['success'=>false, 'message'=>'delete not allowed on this resource'], 403);
        }
        $orm = \ORM::for_table($table);
        $autoWheres = $config->getDeleteAutoWhere();
        foreach($autoWheres as $field=>$value) {
            $orm->where($field,$value);
        }        
        $orm->find_one($id)->delete();
        
        $config->doSleep();

        return $this->app->json(['success'=>true], 200);
    }
    
    public function toggle($table, $id)
    {
        $config = $this->getConfig($table);
        if ($config->doesAllowAction('toggle') === false) {
            return $this->app->json(['success'=>false, 'message'=>'toggle not allowed on this resource'], 403);
        }
        
        $orm = \ORM::for_table($table);
        $autoWheres = $config->getSelectAutoWhere();
        foreach($autoWheres as $field=>$value) {
            $orm->where($field, $value);
        }
        $item = $orm->find_one($id);
        
        $values = $config->getToggleableValues($this->mergeParams());
        foreach($values as $key=>$value) {
            $item->$key = $value;
        }
        
        $autoWheres = $config->getUpdateAutoWhere();
        foreach($autoWheres as $field=>$value) {
            $item->$field = $value;
        }
        
        $item->save();

        $config->doSleep();

        return $this->app->json(['success'=>true, 'id'=>$id, 'data'=>$values], 200);
    }
}