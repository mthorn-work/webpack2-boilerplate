<?php
namespace App;

class RestTable
{
    public $config = [];
    
    public static function __callStatic($table, $parameters) 
    {
        $app = $parameters[0];
        if (isset($app['RestTable__'.$table]) === false) {
            $app['RestTable__'.$table] = new RestTable($parameters[0], $table);
            return $app['RestTable__'.$table];
        } else {
            return $app['RestTable__'.$table];
        }
    }
    
    public function __construct($app, $table) 
    {  
        $this->app = $app;
        $this->config = [
            'sleep'=>0,
            'allowActions'=>[], # 
            'defaultSorting'=>['', ''], # 
            'selectableFields'=>['*'],
            'nonSelectableFields'=>[],
            'searchableFields'=>[],
            'insertableFields'=>['*'],
            'updateableFields'=>['*'],
            'toggleableFields'=>['*'],
            'selectAutoWhere'=>[],
            'insertAutoWhere'=>[],
            'updateAutoWhere'=>[],
            'deleteAutoWhere'=>[],
            'toggleAutoWhere'=>[],
        ];
    }
    
    public function doSleep($override=null)
    {
        $sleepAmount = (is_null($override) === true)?$this->config['sleep']:$override;
        if ($sleepAmount > 0) {
            sleep($sleepAmount);
        }
    }
    
    public function sleep($amount) 
    {
        $this->config['sleep'] = $amount;
    }
    
    public function allowActions() 
    {
        $actions = func_get_args();
        if (isset($actions[0]) === true && $actions[0] == '*') {
            $actions = ['fetchOne', 'fetchMany', 'insert', 'update', 'delete', 'toggle'];
        }
        $this->config['allowActions'] = $actions;
        return $this;
    }
    
    public function defaultSorting($sortColumn, $sortDirection = 'asc')
    {
        $this->config['defaultSorting'] = [$sortColumn, $sortDirection];
        return $this;
    }
    
    public function selectableFields()
    {
        $this->config['selectableFields'] = func_get_args();
        return $this;
    }
    
    public function nonSelectableFields()
    {
        $this->config['nonSelectableFields'] = func_get_args();
        return $this;
    }
    
    public function searchableFields()
    {
        $this->config['searchableFields'] = func_get_args();
        return $this;
    }
    
    public function insertableFields()
    {
        $this->config['insertableFields'] = func_get_args();
        return $this;
    }
    
    public function updateableFields()
    {
        $this->config['updateableFields'] = func_get_args();
        return $this;
    }
    
    public function toggleableFields()
    {
        $this->config['toggleableFields'] = func_get_args();
        return $this;
    }
    
    public function selectAutoWhere($fieldsValues = []) 
    {
        $this->config['selectAutoWhere'] = $fieldsValues;
        return $this;
    }
    
    public function insertAutoWhere($fieldsValues = []) 
    {
        $this->config['insertAutoWhere'] = $fieldsValues;
        return $this;
    }
    
    public function updateAutoWhere($fieldsValues = []) 
    {
        $this->config['updateAutoWhere'] = $fieldsValues;
        return $this;
    }
    
    public function deleteAutoWhere($fieldsValues = []) 
    {
        $this->config['deleteAutoWhere'] = $fieldsValues;
        return $this;
    }
    
    public function toggleAutoWhere($fieldsValues = []) 
    {
        $this->config['toggleAutoWhere'] = $fieldsValues;
        return $this;
    }
    
    public function doesAllowAction($action)
    {
        error_log('allowed actions count: '.count($this->config['allowActions']));
        if (count($this->config['allowActions']) === 0) {
           http_response_code(405);
           exit("{'success':false, 'message':'No actions available on this resource'}");
        }
        return in_array($action, $this->config['allowActions']);
    }
    
    public function getDefaultSorting()
    {
        return $this->config['defaultSorting'];
    }
    
    public function getSelectableFields() 
    {
        return $this->config['selectableFields'];
    }
    
    public function getNonSelectableFields() 
    {
        return $this->config['nonSelectableFields'];
    }
    
    public function getSearchableFields() 
    {
        return $this->config['searchableFields'];
    }
    
    public function getInsertableValues($values) 
    {
        if (isset($this->config['insertableFields'][0]) === true && $this->config['insertableFields'][0] == '*') {
            return $values;
        } else {
           $finalValues = [];
           for ($i=0; $i<count($this->config['insertableFields']); $i++) {
              if (isset($values[$this->config['insertableFields'][$i]]) === true) {
                  $finalValues[$this->config['insertableFields'][$i]] = $values[$this->config['insertableFields'][$i]];
              }
           }
           return $finalValues;
        }
    }
    
    public function getUpdateableValues($values) 
    {
        if (isset($this->config['updateableFields'][0]) === true && $this->config['updateableFields'][0] == '*') {
            return $values;
        } else {
           $finalValues = [];
           for ($i=0; $i<count($this->config['updateableFields']); $i++) {
              if (isset($values[$this->config['updateableFields'][$i]]) === true) {
                  $finalValues[$this->config['updateableFields'][$i]] = $values[$this->config['updateableFields'][$i]];
              }
           }
           return $finalValues;
        }
    }
    
    public function getToggleableValues($values) 
    {
        if (isset($this->config['toggleableFields'][0]) === true && $this->config['toggleableFields'][0] == '*') {
            return $values;
        } else {
           $finalValues = [];
           for ($i=0; $i<count($this->config['toggleableFields']); $i++) {
              if (isset($values[$this->config['toggleableFields'][$i]]) === true) {
                  $finalValues[$this->config['toggleableFields'][$i]] = $values[$this->config['toggleableFields'][$i]];
              }
           }
           return $finalValues;
        }
    }
    
    public function getSelectAutoWhere() 
    {
        return $this->config['selectAutoWhere'];
    }
    
    public function getInsertAutoWhere() 
    {
        return $this->config['insertAutoWhere'];
    }
    
    public function getUpdateAutoWhere() 
    {
        return $this->config['updateAutoWhere'];
    }
    
    public function getDeleteAutoWhere() 
    {
        return $this->config['deleteAutoWhere'];
    }
    
    public function getToggleAutoWhere() 
    {
        return $this->config['toggleAutoWhere'];
    }
}