<?php
namespace App;

class RestHelper 
{
    public function __construct($app) 
    {
        $this->app = $app;
    }
    
    public function handleCorsOptions()
    {
        header('X-Powered-By: Your Mom'); # because reasons
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
            header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Accept');
            exit();
        }
        header('Access-Control-Allow-Origin: *');
    }
    
    public function registerRestRoutes($prefix='')
    {
        $this->app->get($prefix.'/{table}', "App\\RestController::fetchMany");
        $this->app->get($prefix.'/{table}/{id}', "App\\RestController::fetchOne");
        $this->app->post($prefix.'/{table}', "App\\RestController::insertItem");
        $this->app->put($prefix.'/{table}/{id}', "App\\RestController::updateItem");
        $this->app->delete($prefix.'/{table}/{id}', "App\\RestController::deleteItem");
        $this->app->patch($prefix.'/{table}/{id}', "App\\RestController::toggle");
    }
    
    public function registerErrorHandler()
    {
        set_error_handler("App\\RestHelper::errorHandler");
        set_exception_handler("App\\RestHelper::errorHandler");
        register_shutdown_function("App\\RestHelper::errorHandler");
    }
    
    public static function errorHandler()
    {
        global $app;
        $e = func_get_args();
        
        # there will be NO parameters to this if called via register_shutdown_function
        if (count($e) == 0) {
            # in this case, we need to call error_get_last
            $e = error_get_last();
            # if nothing comes back, there was no error. yay!
            if(empty($e) === true) {
                return;
            }
        }
        
        if (is_object($e) === true) {
            $exceptionArray = [];
            $exceptionArray[0] = $e->getCode();
            $exceptionArray[1] = $e->getMessage();
            $exceptionArray[2] = $e->getFile();
            $exceptionArray[3] = $e->getLine();
            $e = $exceptionArray;
        }
        
        if (is_object($e[0]) == true) {
          $error = [
              'code'=>$e[0]->getCode(),
              'message'=>$e[0]->getMessage(),
              'file'=>$e[0]->getFile(),
              'line'=>$e[0]->getLine(),
          ]; 
        } else {
          $error = [
              'code'=>$e[0],
              'message'=>$e[1],
              'file'=>$e[2],
              'line'=>$e[3],
          ];
        }
        
        $msg = 'Error in '.$error['file'].':'.$error['line'].' - '.$error['message'];
        if (is_object($app) === true && isset($app['monolog']) === true && is_object($app['monolog']) === true) {
            $app['monolog']->addError($msg);
        } else {
            error_log($msg);
        }
        
        exit("{'success':false}");
    }
}