<?php
// web/index.php
if (file_exists(__DIR__.'/public'.$_SERVER['REQUEST_URI']) == true) {
    return false;
}

require_once __DIR__.'/../vendor/autoload.php';

global $app;

$app = new Silex\Application();
$restHelper = new \App\RestHelper($app);

$restHelper->handleCorsOptions();
$restHelper->registerRestRoutes('/api/v1');
$restHelper->registerErrorHandler();

App\RestTable::organizations($app)->allowActions('fetchMany');
App\RestTable::users($app)->allowActions('*')->searchableFields('first_name', 'last_name', 'email');
App\RestTable::countries($app)->allowActions('*')->searchableFields('name', 'id');

  #->selectableFields('*')
  #->nonSelectableFields()
  #->insertableFields('*')
  #->updateableFields('*')
  #->selectAutoWhere()
  #->insertAutoWhere()
  #->updateAutoWhere()
  #->deleteAutoWhere(['org_id'=>1]);

$app['debug'] = (getenv('__STAGE__') == 'development');

require_once __DIR__.'/../vendor/j4mie/idiorm/idiorm.php';
ORM::configure(array(
    'connection_string' => 'sqlite:' . (__DIR__.'/../database/database.sqlite'),
    'return_result_sets'=> false,
));

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../debug.log',
));
if ($app['debug'] == true) {
    ORM::configure('logging', true);
    ORM::configure('logger', function($log_string, $query_time)  {
        global $app;
        $app['monolog']->addDebug($log_string . ' in ' . $query_time);
    });
}

# add custom routes here
#$app->get('/api/v1/login', "App\\AuthenticationController::login");
#$app->get('/api/v1/logout', "App\\AuthenticationController::logout");

$app->run();

$app['monolog']->addDebug('---------------------------------------------------');
