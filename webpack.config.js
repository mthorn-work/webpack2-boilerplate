
const webpack = require('webpack');
const path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const nodeEnv = process.env.NODE_ENV || 'development';
const isProd = process.argv.indexOf('-p') !== -1;

const sourcePath = path.join(__dirname, './client/web');
const staticsPath = path.join(__dirname, './server/public');

const presets = [["es2015", { "modules": false }], "react", "stage-2"];
const resolveAliases = {};

const scssLoader = {
  test: /\.scss$/
};

const plugins = [
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks: Infinity,
    filename: 'vendor.bundle.js'
  }),
  new webpack.DefinePlugin({
    'process.env': { NODE_ENV: JSON.stringify(nodeEnv) }
  }),
  new webpack.NamedModulesPlugin(),
];

if (isProd) {
  console.log('building production!');
  
  resolveAliases['react'] = 'react-lite';
  resolveAliases['react-dom'] = 'react-lite';
  
  plugins.push(
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true,
      },
      output: {
        comments: false
      },
      sourceMap:true
    }),
    new ExtractTextPlugin("styles.css")
  );
  scssLoader.loader = ExtractTextPlugin.extract({
    fallbackLoader: "style-loader",
    loader: "css-loader?modules&camelCase&localIdentName=[local]!sass-loader",
  });
  
} else {
  console.log('building development!');
  presets.push('react-hmre');
  plugins.push(
    new webpack.HotModuleReplacementPlugin()
  );
  scssLoader.loaders = ["style-loader", "css-loader?modules&camelCase&localIdentName=[local]", "sass-loader"];
}

module.exports = {
  devtool: isProd ? 'source-map' : 'eval',
  context: sourcePath,
  entry: {
    js: './index.js',
    vendor: ['react']
  },
  output: {
    path: staticsPath,
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        exclude: /node_modules/,
        use: 'file-loader',
        query: {
          name: '[name].[ext]'
        }
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        include: __dirname,
        'query':{
            presets: presets
        }
      },
      {
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/,
        loader: "file-loader"
      },
      scssLoader  // this is different in prod vs dev. 
    ],
  },
  resolve: {
    extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js', '.jsx'],
    modules: [
      path.resolve(__dirname, 'node_modules'),
      sourcePath
    ],

    alias: resolveAliases
  
  },
  plugins,
  devServer: {
    contentBase: sourcePath,
    historyApiFallback: true,
    port: 3000,
    compress: isProd,
    inline: !isProd,
    hot: !isProd,
    stats: {
      assets: true,
      children: false,
      chunks: false,
      hash: false,
      modules: false,
      publicPath: false,
      timings: true,
      version: false,
      warnings: true,
      colors: {
        green: '\u001b[32m',
      }
    },
  }
};